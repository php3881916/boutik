<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240713095651 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE boutik.product_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE boutik.tva_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE boutik.product (id INT NOT NULL, category_id INT DEFAULT NULL, tva_id INT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, description TEXT NOT NULL, illustration VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_368771C912469DE2 ON boutik.product (category_id)');
        $this->addSql('CREATE INDEX IDX_368771C94D79775F ON boutik.product (tva_id)');
        $this->addSql('CREATE TABLE boutik.tva (id INT NOT NULL, name VARCHAR(255) NOT NULL, rate DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE boutik.product ADD CONSTRAINT FK_368771C912469DE2 FOREIGN KEY (category_id) REFERENCES boutik.category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE boutik.product ADD CONSTRAINT FK_368771C94D79775F FOREIGN KEY (tva_id) REFERENCES boutik.tva (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE boutik.product_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE boutik.tva_id_seq CASCADE');
        $this->addSql('ALTER TABLE boutik.product DROP CONSTRAINT FK_368771C912469DE2');
        $this->addSql('ALTER TABLE boutik.product DROP CONSTRAINT FK_368771C94D79775F');
        $this->addSql('DROP TABLE boutik.product');
        $this->addSql('DROP TABLE boutik.tva');
    }
}
