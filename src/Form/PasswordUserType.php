<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Constraints\Length;

class PasswordUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('actualPassword',PasswordType::class, [
                'label' => 'Votre mot de passe actuel',
                    'attr'=> [
                        'placeholder'=>'Vérification du mot de passe'
                    ],
                'mapped'=> false,
            ])
            ->add('plainPassword',RepeatedType::class,[
                'type' => PasswordType::class,
                'constraints'=>[
                    new Length([
                        'min' => 2,
                        'max' => 30
                    ])
                ],
                'first_options'  => [
                    'label' => 'Votre nouveau mot de passe',
                    'hash_property_path' => 'password',
                    'attr'=> [
                        'placeholder'=>'Saisissez le nouveau mot de passe'
                    ]
                ],
               'second_options' => [
                    'label' => 'Confirmer le nouveau mot de passe',
                    'attr'=> [
                        'placeholder'=>'Confirmation du nouveau mot de passe'
                    ]
                ],
                'mapped'=> false,
            ])
            ->add('submit', SubmitType::class,[
                'label'=> 'Modifier le mot de passe',
                'attr'=> [
                    'class'=>'btn btn-success'
                ]
            ])
            ->addEventListener(FormEvents::SUBMIT, function(FormEvent $event) {
                $user = $event->getData();
                $form = $event->getForm();
                $actualPassword = $form->get('actualPassword')->getData();
                
                $passwordHasher = $form->getConfig()->getOptions()['passwordHasher'];

                if (!$passwordHasher->isPasswordValid($user,$actualPassword)) {
                    $form->get('actualPassword')->addError(new FormError('Votre mot de passe actuel est incorrect'));
                }

            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'passwordHasher'=> null
        ]);
    }
}
