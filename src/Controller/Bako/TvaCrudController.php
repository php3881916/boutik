<?php

namespace App\Controller\Bako;

use App\Entity\Tva;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;

class TvaCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Tva::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('TVA')
            ->setEntityLabelInPlural('TVA')
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name')->setLabel('Nom'),
            NumberField::new('rate')->setLabel('Taux'),
        ];
    }
}
