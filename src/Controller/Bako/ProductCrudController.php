<?php

namespace App\Controller\Bako;

use App\Constantes;
use App\Entity\Product;
use App\Repository\TvaRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ProductCrudController extends AbstractCrudController
{

private TvaRepository $tvaRepository;

    public function __construct(TvaRepository $repository)
    {
        $this->tvaRepository = $repository;   
    }
    public static function getEntityFqcn(): string
    {
        return Product::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Produit')
            ->setEntityLabelInPlural('Produits')
        ;
    }

    public function createEntity(string $entityFqcn)
    {
        $product = new Product();
        $tva = $this->tvaRepository->findOneBy(['name'=>Constantes::TVA_NOMINALE]);
        $product->setTva($tva);

        return $product;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name')->setLabel('Nom')->setHelp('Nom de votre produit'),
            SlugField::new('slug')->setTargetFieldName('name')->setLabel('Url')->setHelp('Personnalisable avec le cadenas'),
            AssociationField::new('category')->setLabel('Categorie')->setHelp('Catégorie du produit'),
            NumberField::new('price')->setLabel('Prix HT')->setHelp('Prix HT du produit sans unité'),
            AssociationField::new('tva')->setLabel('Type de TVA')->setHelp('Choisissez le taux de tva'),
            ImageField::new('illustration')
                ->setLabel('Image')
                ->setHelp('Image du produit en 600x600px')
                ->setUploadDir('public/uploads')
                ->setBasePath('uploads')
                ->setUploadedFileNamePattern('[year]-[month]-[day]-[contenthash].[extension]'),
            TextEditorField::new('description')->setLabel('Description'),
        ];
    }
}
